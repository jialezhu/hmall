package com.hmall.common.dto.clients;

import com.hmall.common.dto.pojo.Address;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("userservice")
@Component
public interface UserClient {

    @GetMapping("/address/uid/{userId}")
    List<Address> getById(@PathVariable Long userId);

    @GetMapping("/address/{id}")
    Address findAddressById(@PathVariable Long id);
}
