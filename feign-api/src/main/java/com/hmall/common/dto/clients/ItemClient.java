package com.hmall.common.dto.clients;

import com.hmall.common.dto.PageDTO;
import com.hmall.common.dto.pojo.Item;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;


@FeignClient("itemservice")
@Component
public interface ItemClient {

    @GetMapping("/item/{id}")
    Item getById(@PathVariable Long id);

    @PutMapping("item")
    void update(@RequestBody Item item);

    @GetMapping("/item/list")
    PageDTO<Item> page(@RequestParam("page") Integer page, @RequestParam("size") Integer size);



}
