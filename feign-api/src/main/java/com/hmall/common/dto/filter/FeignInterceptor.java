package com.hmall.common.dto.filter;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;

@Component
public class FeignInterceptor implements RequestInterceptor {
    //为每一个通过feign的微服务的请求添加请求头
    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("authorization", "2");
    }
}
