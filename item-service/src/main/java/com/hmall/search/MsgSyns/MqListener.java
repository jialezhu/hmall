package com.hmall.search.MsgSyns;

import com.alibaba.fastjson.JSON;
import com.hmall.search.Item;
import com.hmall.search.ItemDoc;
import com.hmall.search.service.IItemService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class MqListener {
    @Autowired
    private IItemService itemService;
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @RabbitListener(queues = "insert.queue")
    public void insertListener(Long id) {
        try {
            Item item = itemService.getById(id);
            ItemDoc itemDoc = new ItemDoc(item);
            IndexRequest indexRequest = new IndexRequest("heimahotel").id(String.valueOf(id));
            indexRequest.source(JSON.toJSONString(itemDoc), XContentType.JSON);
            restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("es商品更新,新增失败");
            throw new RuntimeException(e);
        }
    }

    @RabbitListener(queues = "delete.queue")
    public void deletelistener(Long id) {
        try {
            DeleteRequest deleteRequest = new DeleteRequest("heimahotel").id(String.valueOf(id));
            restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("es商品删除失败");
         throw new RuntimeException(e);
        }

    }

}
