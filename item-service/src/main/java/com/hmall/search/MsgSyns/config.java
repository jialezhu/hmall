package com.hmall.search.MsgSyns;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class config {
    //item 交换机
    @Bean
    public TopicExchange topicExchange() {
        return ExchangeBuilder.topicExchange("item.exchange").build();
    }

    //item 新增修改队列
    @Bean
    public Queue insertQunue() {
        return QueueBuilder.durable("insert.queue").build();
    }

    // item 删除队列
    @Bean
    public Queue deleteQunue() {
        return QueueBuilder.durable("delete.queue").build();
    }

    // item 新增和修改绑定交换机
    @Bean
    public Binding insertbing() {
        return BindingBuilder.bind(insertQunue()).to(topicExchange()).with("insert");
    }

    // item 删除绑定交换机
    @Bean
    public Binding deletebing() {
        return BindingBuilder.bind(deleteQunue()).to(topicExchange()).with("delete");
    }
}
