package com.hmall.search.web;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmall.common.dto.PageDTO;
import com.hmall.search.Item;
import com.hmall.search.service.IItemService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("item")
public class ItemController {

    @Autowired
    private IItemService itemService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    //分页查询
    @GetMapping("/list")
    public PageDTO<Item> page(Integer page, Integer size) {
        Page<Item> page1 = itemService.page(new Page<>(page, size));
        PageDTO<Item> pageDto = new PageDTO<>();
        pageDto.setTotal(page1.getTotal());
        pageDto.setList(page1.getRecords());
        return pageDto;
    }

    //修改状态
    @PutMapping("/status/{id}/{status}")
    public Item updateStatus(@PathVariable Long id, @PathVariable Integer status) {
        Item item = new Item();
        item.setId(id);
        item.setStatus(status);
        item.setUpdateTime(new Date());
        itemService.updateById(item);
        return item;

    }

    //根据id查找回显
    @GetMapping("/{id}")
    public Item selecetById(@PathVariable Long id) {
        return itemService.getById(id);
    }

    //新增
    @PostMapping
    public void save(@RequestBody Item item) {
        item.setCreateTime(new Date());
        String spec = item.getSpec();
        item.setSpec("{\"规格\":\"" + spec + "\"}");
        itemService.save(item);
        rabbitTemplate.convertAndSend("item.exchange", "insert", item.getId());
    }

    //修改
    @PutMapping
    public void update(@RequestBody Item item) {
        String spec = item.getSpec();
        if (spec != null) {
            item.setSpec("{\"规格\":\"" + spec + "\"}");
        }
        itemService.updateById(item);
        rabbitTemplate.convertAndSend("item.exchange", "insert", item.getId());
    }

    //删除
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        itemService.removeById(id);
        rabbitTemplate.convertAndSend("item.exchange", "delete", id);
    }
}


