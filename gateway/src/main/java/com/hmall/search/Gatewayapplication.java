package com.hmall.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gatewayapplication {
    public static void main(String[] args) {
        SpringApplication.run(Gatewayapplication.class, args);
    }
}
