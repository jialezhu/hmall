package com.hmall.order.filter;

import com.hmall.order.context.BaseContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class Interceptor implements HandlerInterceptor {
    //拦截器
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String authorizationHeader = request.getHeader("authorization");
        Long I = Long.valueOf(authorizationHeader);
        // 将用户ID保存到ThreadLocal中
        BaseContext.setUserId(I);

        return true; // 继续处理请求
    }
}
