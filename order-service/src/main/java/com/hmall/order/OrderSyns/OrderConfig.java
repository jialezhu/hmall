package com.hmall.order.OrderSyns;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderConfig {
    //死信交换机
    @Bean
    public DirectExchange exchange() {
        return ExchangeBuilder.directExchange("order.exchange")
                .delayed()
                .durable(true)
                .build();
    }
    //死信队列
    @Bean
    public Queue queue() {
        return QueueBuilder.durable("order.queue").build();
    }
    //死信队列与死信交换机绑定
    @Bean
    public Binding orderBing() {
        return BindingBuilder.bind(queue()).to(exchange()).with("order");
    }
}
