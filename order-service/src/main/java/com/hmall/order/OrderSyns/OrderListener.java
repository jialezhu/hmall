package com.hmall.order.OrderSyns;

import com.hmall.order.mapper.ItemMapper;
import com.hmall.order.mapper.OrderDetailMapper;
import com.hmall.order.mapper.OrderMapper;
import com.hmall.order.pojo.Item;
import com.hmall.order.pojo.Order;
import com.hmall.order.pojo.OrderDetail;
import com.hmall.order.service.impl.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class OrderListener {
    @Autowired
    private OrderDetailMapper orderDetailMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private ItemMapper itemMapper;

    @RabbitListener(queues = "order.queue")
    public void rollback(Long id) {
        OrderDetail orderDetail = orderDetailMapper.seletebyIds(id);
        Integer num = orderDetail.getNum();
        Long itemId = orderDetail.getItemId();
        Order order = orderMapper.selectById(id);
        if (order.getStatus() != 2) {
            order.setStatus(5);
        }
        Item item = itemMapper.selectById(itemId);
        Integer stock = item.getStock();
        int newStock = stock + num;
        item.setStock(newStock);
        Integer sold = item.getSold();
        int newsold = sold - num;
        item.setSold(newsold);
        itemMapper.updateById(item);
        Integer stock1 = item.getStock();
        System.out.println(stock1);
    }
}
