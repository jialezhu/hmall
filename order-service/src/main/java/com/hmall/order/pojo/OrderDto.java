package com.hmall.order.pojo;

import lombok.Data;

@Data
public class OrderDto {
    private Integer num;
    private Integer paymentType;
    private Integer addressId;
    private Long itemId;
}
