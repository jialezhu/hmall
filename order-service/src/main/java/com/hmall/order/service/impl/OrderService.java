package com.hmall.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmall.common.dto.clients.ItemClient;
import com.hmall.common.dto.clients.UserClient;
import com.hmall.common.dto.pojo.Address;
import com.hmall.common.dto.pojo.Item;
import com.hmall.order.context.BaseContext;
import com.hmall.order.mapper.OrderDetailMapper;
import com.hmall.order.mapper.OrderLogsMapper;
import com.hmall.order.mapper.OrderMapper;
import com.hmall.order.pojo.Order;
import com.hmall.order.pojo.OrderDetail;
import com.hmall.order.pojo.OrderDto;
import com.hmall.order.pojo.OrderLogistics;
import com.hmall.order.service.IOrderService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
@Slf4j
public class OrderService extends ServiceImpl<OrderMapper, Order> implements IOrderService {
    @Autowired
    private ItemClient itemClient;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderDetailMapper orderDetailMapper;
    @Autowired
    private UserClient userClient;
    @Autowired
    private OrderLogsMapper orderLogsMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    @GlobalTransactional(rollbackFor = Exception.class)
    public String sumbitOrder(OrderDto orderDto) {
        //将信息存入存入Order表中
        Item item = itemClient.getById(orderDto.getItemId());
        Order order = new Order();
        order.setUserId(BaseContext.getUserId());
        Long price = item.getPrice();
        Integer num = orderDto.getNum();
        order.setTotalFee(price * num);
        order.setPaymentType(orderDto.getPaymentType());
        order.setCreateTime(new Date());
        order.setStatus(1);
        orderMapper.insert(order);
        //封装 OrderDetail 存入表中
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setOrderId(order.getId());
        orderDetail.setItemId(item.getId());
        orderDetail.setNum(orderDto.getNum());
        orderDetail.setName(item.getName());
        orderDetail.setPrice(item.getPrice());
        orderDetail.setSpec(item.getSpec());
        orderDetail.setImage(item.getImage());
        orderDetail.setCreateTime(new Date());
        orderDetailMapper.insert(orderDetail);
        //封装OrderLogistics 存入表中
        Address addressById = userClient.findAddressById(new Long(orderDto.getAddressId()));
        OrderLogistics orderLogistics = new OrderLogistics();
        orderLogistics.setOrderId(order.getId());
        orderLogistics.setContact(addressById.getContact());
        orderLogistics.setMobile(addressById.getMobile());
        orderLogistics.setProvince(addressById.getProvince());
        orderLogistics.setCity(addressById.getCity());
        orderLogistics.setTown(addressById.getTown());
        orderLogistics.setStreet(addressById.getStreet());
        orderLogistics.setCreateTime(new Date());
        orderLogsMapper.insert(orderLogistics);
        //在orderdetail 插入之后 发送延迟消息到队列
        rabbitTemplate.convertAndSend("order.exchange", "order", order.getId(),
                message -> {
                    message.getMessageProperties().setHeader("x-delay", 1);
                    return message;
                });
        //更新库存 销量
        Integer stock = item.getStock();
        int stocks = stock - num;
        item.setStock(stocks);
        Integer sold = item.getSold();
        sold = sold + num;
        item.setSold(sold);
        itemClient.update(item);
        return order.getId().toString();
    }

    @Override
    public Order getbyids(String longordeId) {
        return orderMapper.getbyids(longordeId);
    }
}
