package com.hmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmall.order.pojo.Order;
import com.hmall.order.pojo.OrderDto;

public interface IOrderService extends IService<Order> {
    String sumbitOrder(OrderDto orderDto);

    Order getbyids(String longordeId);
}
