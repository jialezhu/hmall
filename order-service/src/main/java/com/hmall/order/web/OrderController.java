package com.hmall.order.web;

import com.hmall.order.pojo.Order;
import com.hmall.order.pojo.OrderDto;
import com.hmall.order.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/order")
@Transactional
public class OrderController {

    @Autowired
    private IOrderService orderService;


    @GetMapping("/{id}")
    public Order queryOrderById(@PathVariable("id") Long orderId) {
        String substring = orderId.toString().substring(0, orderId.toString().length() - 5);
        return orderService.getbyids(substring);
    }

    @PostMapping
    public String createOrder(@RequestBody OrderDto orderDto) {
        return orderService.sumbitOrder(orderDto);
    }


}
