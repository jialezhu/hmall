package com.hmall.user.context;

public class BaseContext {

        // 使用ThreadLocal来存储用户ID
        private static final ThreadLocal<Long> threadLocal = new ThreadLocal<>();

        // 设置用户ID到ThreadLocal中
        public static void setUserId(Long userId) {
            threadLocal.set(userId);
        }

        // 从ThreadLocal中获取用户ID
        public static Long getUserId() {
            return threadLocal.get();
        }

    }
