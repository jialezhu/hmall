package com.hmall.user.filter;

import com.hmall.common.dto.filter.FeignInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {
    @Bean("FeignInterceptor")
    public FeignInterceptor interceptor() {
        return new FeignInterceptor();
    }
}
