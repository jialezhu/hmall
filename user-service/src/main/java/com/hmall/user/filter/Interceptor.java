package com.hmall.user.filter;


import com.hmall.user.context.BaseContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class Interceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String authorizationHeader = request.getHeader("authorization");
        // 将用户ID保存到ThreadLocal中
        BaseContext.setUserId(Long.valueOf(authorizationHeader));
        return true; // 继续处理请求
    }
}
