package com.hmall.search.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmall.search.config.PageResult;
import com.hmall.search.pojo.Item;
import com.hmall.search.pojo.ItemDoc;
import com.hmall.search.pojo.PageDTO;
import com.hmall.search.pojo.PageQuryDto;

import java.util.List;
import java.util.Map;

public interface SearchService  extends IService<Item> {

    PageDTO<ItemDoc> listdate(PageQuryDto pageQuryDto);

    Map<String, List<String>> filters(PageQuryDto pageQuryDto);

    List<String> suggestion(String key);
}
