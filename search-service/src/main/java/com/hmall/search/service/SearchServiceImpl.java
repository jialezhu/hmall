package com.hmall.search.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmall.search.mapper.SearchMapper;
import com.hmall.search.pojo.Item;
import com.hmall.search.pojo.ItemDoc;
import com.hmall.search.pojo.PageDTO;
import com.hmall.search.pojo.PageQuryDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SearchServiceImpl extends ServiceImpl<SearchMapper, Item> implements SearchService {
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Override
    public PageDTO<ItemDoc> listdate(PageQuryDto pageQuryDto) {
        String key = pageQuryDto.getKey();
        int page = Objects.isNull(pageQuryDto.getPage()) ? 1 : pageQuryDto.getPage();
        int size = Objects.isNull(pageQuryDto.getSize()) ? 20 : pageQuryDto.getSize();
        Integer maxPrice = pageQuryDto.getMaxPrice();
        Integer minPrice = pageQuryDto.getMinPrice();
        String brand = pageQuryDto.getBrand();
        String sortBy = pageQuryDto.getSortBy();
        String category = pageQuryDto.getCategory();
        try {
            int from = (page - 1) * size;
            SearchRequest searchRequest = new SearchRequest("heimahotel");
            BoolQueryBuilder boolQuery = getBoolQueryBuilder(key, maxPrice, minPrice, brand, category);
            //设置排序的的搜索结果
            if (!Objects.equals("default", sortBy)) {
                searchRequest.source().sort(
                        pageQuryDto.getSortBy(), SortOrder.DESC);
            }
            //设置高亮的查询条件
            searchRequest.source().highlighter(new HighlightBuilder()
                    .field("all") //设置高亮的字段
                    .preTags("<span style=\"color: purple\">")
                    .postTags("</span>")
                    .requireFieldMatch(true)//当高亮的字段不为all是要false
            );
            //设置搜索请求的查询条件
            searchRequest.source().query(boolQuery);
            //分页
            searchRequest.source().from(from).size(size);
            SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            //获得搜索结果集
            SearchHits hits = search.getHits();
            //获取命中的总文档个数
            TotalHits totalHits = hits.getTotalHits();
            //获取具体的搜索结果数组，每个SearchHit代表一个匹配搜索条件的文档。
            SearchHit[] hitsHits = hits.getHits();
            //利用stream流将每个文档的数据的JSON字符串 并反序列化为对象最后在转换为集合
            List<ItemDoc> collect = Arrays.stream(hitsHits).map(hit -> {
                String sourceAsString = hit.getSourceAsString();
                ItemDoc itemDoc = JSON.parseObject(sourceAsString, ItemDoc.class);
                //获取高亮字段的集合
                Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                //获取字段名中为all的字段的highlightField对象
                HighlightField highlightField = highlightFields.get("all");
                if (Objects.nonNull(highlightField)) {
                    //从highlightField对象中获取高亮片段数组
                    Text[] fragments = highlightField.getFragments();
                    for (Text fragment : fragments) {
                        //遍历高亮片段数组，将每个片段转换为字符串，并设置到itemDoc对象的name
                        itemDoc.setName(fragment.toString());
                    }
                }
                return itemDoc;
            }).collect(Collectors.toList());
            PageDTO<ItemDoc> itemDocPageDTO = new PageDTO<>();
            itemDocPageDTO.setList(collect);
            itemDocPageDTO.setTotal(totalHits.value);

            return itemDocPageDTO;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }


    @Override
    public Map<String, List<String>> filters(PageQuryDto pageQuryDto) {
        String key = pageQuryDto.getKey();
        Integer maxPrice = pageQuryDto.getMaxPrice();
        Integer minPrice = pageQuryDto.getMinPrice();
        String brand = pageQuryDto.getBrand();
        String category = pageQuryDto.getCategory();
        //根据索引名构建搜索请求
        SearchRequest searchRequest = new SearchRequest("heimahotel");
        //先设置搜索请求的查询条件 再添加一个聚合操作 trems 是聚合的名字 field是聚合的字段
        searchRequest.source().query(getBoolQueryBuilder(key, maxPrice, minPrice, brand, category))
                .aggregation(AggregationBuilders.terms("category").field("category").size(20))
                .aggregation(AggregationBuilders.terms("brand").field("brand").size(20));

        try {
            SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            //从es搜索中获取聚合结果
            Aggregations aggregations = search.getAggregations();
            //获取 字段的聚合结果  类型要转换为 ParsedStringTerms
            ParsedStringTerms keybrand = aggregations.get("brand");
            ParsedStringTerms keycategory = aggregations.get("category");
            // 从字段的聚合结果来获得桶  每个桶内都有这个字段的唯一名称和字段内的文档的计数
            List<? extends Terms.Bucket> brandbuckets = keybrand.getBuckets();
            List<? extends Terms.Bucket> categorybuckets = keycategory.getBuckets();
            //再根据这个桶内的字段（也就是各个品牌的名称）个数根据Stream流转换为List<String>
            List<String> brandbucket = brandbuckets.stream().map(item -> item.getKeyAsString()).collect(Collectors.toList());
            List<String> categorybucket = categorybuckets.stream().map(item -> item.getKeyAsString()).collect(Collectors.toList());

            HashMap<String, List<String>> stringListHashMap = new HashMap<>();
            stringListHashMap.put("brand", brandbucket);
            stringListHashMap.put("category", categorybucket);


            return stringListHashMap;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        throw new RuntimeException();
    }

    @Override
    public List<String> suggestion(String key) {


        try {
            SearchRequest searchRequest = new SearchRequest("heimahotel");
            //构建搜索建议的自动补全
            searchRequest.source().suggest(new SuggestBuilder()
                    .addSuggestion("suggestions",//添加自动补全建议  名称自己起
                            SuggestBuilders//创建建议
                                    .completionSuggestion("suggestion")
                                    .prefix(key)//设置建议的前缀，即根据用户输入的关键字
                                    .size(10).//设置返回值的大小
                                    skipDuplicates(true)//跳过重复的建议项
                    ));

            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            //得到建议对象
            Suggest suggest = searchResponse.getSuggest();
            //从Suggest对象中获取具体的CompletionSuggestion对象  里面的名称是上面的搜索建议自己定义的
            CompletionSuggestion suggestion = suggest.getSuggestion("suggestions");
            //获取建议选项的列表
            List<CompletionSuggestion.Entry.Option> options = suggestion.getOptions();
            //在通过stream流将列表内每一项转换为文本对象 最后在收集到一个新的List<String>中
            return options.stream().map(item -> item.getText().toString()).collect(Collectors.toList());

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    // 构建复合查询条件
    private BoolQueryBuilder getBoolQueryBuilder(String key, Integer maxPrice, Integer minPrice, String brand, String category) {
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (StringUtils.isNotEmpty(key)) {
            boolQuery.must(QueryBuilders.matchQuery("all", key));
        } else {
            boolQuery.must(QueryBuilders.matchAllQuery());
        }
        if (StringUtils.isNotEmpty(brand)) {
            boolQuery.filter(QueryBuilders.termQuery("brand", brand));
        }
        if (StringUtils.isNotEmpty(category)) {
            boolQuery.filter(QueryBuilders.termQuery("category", category));
        }
        if (Objects.nonNull(maxPrice) && Objects.nonNull(minPrice)) {
            boolQuery.filter(QueryBuilders.rangeQuery("price")
                    .gte(minPrice * 100).lte(maxPrice * 100));
        }
        return boolQuery;
    }

}
