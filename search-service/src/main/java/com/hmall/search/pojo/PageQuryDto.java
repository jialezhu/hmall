package com.hmall.search.pojo;

import lombok.Data;

@Data
public class PageQuryDto {
    /**
     * 钥匙
     */
    private String key;

    /**
     * 页面
     */
    private Integer page;

    /**
     * 尺寸
     */
    private Integer size;

    /**
     * 排序依据
     */
    private String category;

    /**
     * 城市
     */
    private String brand;


    private Integer minPrice;

    /**
     * 最高价格
     */
    private Integer maxPrice;

    private Integer Price;


    private Integer sold;


    private String sortBy;
}
