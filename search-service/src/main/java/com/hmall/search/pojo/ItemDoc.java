package com.hmall.search.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.elasticsearch.index.query.QueryBuilders;

import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
public class ItemDoc implements Serializable {

    private Long id;//商品id
    private String name;//商品名称
    private Long price;//价格（分）
    private Integer stock;//库存数量
    private String image;//商品图片
    private String category;//分类名称
    private String brand;//品牌名称
    private String spec;//规格
    private Integer commentCount;//评论数
    private Boolean isAD;
    private List<String> suggestion;

    public ItemDoc(Item item) {
        this.id = item.getId();
        this.name = item.getName();
        this.isAD = item.getIsAD();
        this.brand = item.getBrand();
        this.price = item.getPrice();
        this.stock = item.getStock();
        this.image = item.getImage();
        this.category = item.getCategory();
        this.spec = item.getSpec();
        this.commentCount = item.getCommentCount();
        this.suggestion = Arrays.asList(this.name, this.brand);


    }
}