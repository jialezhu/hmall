package com.hmall.search.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmall.search.pojo.Item;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SearchMapper extends BaseMapper<Item> {
    
}
