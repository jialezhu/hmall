package com.hmall.search.controller;

import com.hmall.common.dto.clients.ItemClient;
import com.hmall.common.dto.pojo.Item;
import com.hmall.search.config.PageResult;
import com.hmall.search.pojo.ItemDoc;
import com.hmall.search.pojo.PageDTO;
import com.hmall.search.pojo.PageQuryDto;
import com.hmall.search.service.SearchService;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/search")
public class SearchController {
    @Autowired
    private SearchService searchService;


    @PostMapping("/list")
    public PageDTO<ItemDoc> list(@RequestBody PageQuryDto pageQuryDto) {
        return searchService.listdate(pageQuryDto);

    }

    @PostMapping("/filters")
    public Map<String, List<String>> filters(@RequestBody PageQuryDto pageQuryDto) {
        return searchService.filters(pageQuryDto);
    }

    @GetMapping("/suggestion")
    public List<String> suggestion(String key) {

        return searchService.suggestion(key);
    }
}
