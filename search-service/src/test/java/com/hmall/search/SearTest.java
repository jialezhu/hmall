package com.hmall.search;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmall.search.mapper.SearchMapper;
import com.hmall.search.pojo.Item;
import com.hmall.search.pojo.ItemDoc;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class SearTest {
    @Autowired
    private SearchMapper searchMapper;
    @Autowired
    private RestHighLevelClient restHighLevelClient;
    private final static ThreadPoolExecutor EXECUTOR = new ThreadPoolExecutor(
            17,
            50,
            10,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(50)
    );
    @Test
    void Test() throws IOException, InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Long count = searchMapper.selectCount(null);
        int pageSize = 1000;
        int page = (int) (count / pageSize);
        if (count % pageSize > 0) {
            page++;
        }
        // 10 9 8 7 6 5 4 3 2 1 0
        CountDownLatch countDownLatch = new CountDownLatch(page);
        for (int i = 0; i < page; i++) {
            int finalI = i;
            EXECUTOR.execute(() -> {
                try {
                    Page<Item> hotelPage = searchMapper.selectPage(
                            new Page<>(finalI, pageSize, false),
                            null);
                    BulkRequest request = new BulkRequest();
                    // 查询数据库所有信息
                    List<Item> hotels = hotelPage.getRecords();
                    hotels.stream()
                            .map(ItemDoc::new)
                            .forEach(hotelDoc -> {
                                // 构建单个请求
                                IndexRequest indexRequest = new IndexRequest("heimahotel").id(hotelDoc.getId().toString());
                                indexRequest.source(JSON.toJSONString(hotelDoc), XContentType.JSON);
                                // 构建批次请求
                                request.add(indexRequest);
                            });

                    restHighLevelClient.bulk(request, RequestOptions.DEFAULT);
                    countDownLatch.countDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        // 0 阻塞
        countDownLatch.await();
        stopWatch.stop();
        System.out.println("执行任务时间为:" + stopWatch.getTotalTimeMillis() + "毫秒");
    }
}
